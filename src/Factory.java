
public interface Factory {
	public State createState(String type);
	public Transition createTransition(String type);
	public Uml_Object createUmlObejct(String type);
}
